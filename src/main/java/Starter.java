import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Starter extends Thread {
    private static ArrayList<Company> companies = new ArrayList<Company>();
    @Override
    public void run() {
        int threadsQuantity = 30;
        int lastId = 90600;
        int firstId = 89544;

        List<Thread> threads = new ArrayList<Thread>();
        for (int i = lastId; i>firstId; i=i-lastId/threadsQuantity){
            Thread thread = new ParseThread(i, threadsQuantity, lastId, this);
            thread.start();
            threads.add(thread);
        }

        for (Thread thread : threads){
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        sortList(companies);
        makeExcel(companies);
    }

    //Сортировка списка
    private void sortList(ArrayList<Company> companies){
        for (int i = 0; i<companies.size(); i++){
            for (int j = 0; j<companies.size(); j++){
                if (companies.get(i).getNumber()>companies.get(j).getNumber()){
                    Company temp = companies.get(i);
                    companies.set(i, companies.get(j));
                    companies.set(j, temp);
                }
            }
        }
    }

    //Добавление списка компаний в общий список
    public synchronized void addCompanies(List<Company> companies, int index){
        this.companies.addAll(companies);
    }


    //Записываем в файл
    private static void makeExcel(List<Company> companies){
        HSSFWorkbook book = new HSSFWorkbook();
        Sheet sheet = book.createSheet("Builders");
        addTitles(sheet);
        addData(sheet, companies);

        try(FileOutputStream fos = new FileOutputStream("1.xls"))  {
            book.write(fos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Заголовки столбцов в лист
    private static void addTitles(Sheet sheet){
        Row row = sheet.createRow(0);
        String[] titleCells = {"Регион", "Название", "Телефон", "Описание", "URL", "Примечания"};
        int i = 0;
        for (String str : titleCells){
            Cell cell = row.createCell(i++);
            cell.setCellValue(str);
        }
    }

    // Заносим фирмы в лист
    private static void addData(Sheet sheet, List<Company> companies){
        int i = 1;
        for (Company com : companies){
            Row row = sheet.createRow(i++);
            row.createCell(0).setCellValue(com.getRegion());
            row.createCell(1).setCellValue(com.getName());
            row.createCell(2).setCellValue(com.getTelephone());
            row.createCell(3).setCellValue(com.getDescription());
            row.createCell(4).setCellValue(com.getUrl());
        }
    }
}
