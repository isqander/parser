import java.util.Date;

public class Main  {

    public static void main(String[] args) {

        Starter starter = new Starter();
        Date t1 = new Date();
        starter.run();
        System.out.println(new Date().getTime()-t1.getTime());
    }
}
