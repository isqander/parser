import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParseThread extends Thread {
    private List<Company> companies = new ArrayList<Company>();
    private int range;
    private int threadsQuantity;
    private int lastId;
    private Starter starter;

    public ParseThread(int range, int threadsQuantity, int lastId, Starter starter) {
        this.range = range;
        this.threadsQuantity = threadsQuantity;
        this.lastId = lastId;
        this.starter = starter;
        //this.run();
    }

    public void run() {
        for (int i = range; i > range-lastId/threadsQuantity; i--) {
            String url = "http://stroiman.ru/master/card/" + i;
            if (companyExists(url)) {
                try {
                    companies.add(makeCompany(url, i));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        starter.addCompanies(companies, lastId-range);
    }

    public List<Company> getCompanies() {
        return companies;
    }

    //существование фирмы по указанному url
    public static boolean companyExists(String url) {
        try {
            Document doc = Jsoup.connect(url).get();
            if (doc.getElementsByAttributeValue("class", "builders-full__phone hidden-overflow display-flex flex-wrap").size() == 0) {
                return false;
            } else return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    //Собираем данные в Company
    public static Company makeCompany(String url, int i) throws IOException {

        Document doc = Jsoup.connect(url).get();

        String description = doc.getElementsByAttributeValue("class", "builders-full__text hidden-overflow").text();

        String region = doc.getElementsByAttributeValue("class", "builders-full__region hidden-overflow display-flex flex-wrap").text();

        Elements phoneNameElements = doc.getElementsByAttributeValue("class", "builders-full__phone hidden-overflow display-flex flex-wrap");
        String phone = phoneNameElements.get(0).child(1).text();

        String name = phoneNameElements.get(0).child(2).text();

        return new Company(name, phone, region, description, url, i);
    }

}